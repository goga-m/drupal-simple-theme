
ABOUT SIMPLE
-----------
It is just a lean theme (not a bootstrap subtheme) for Drupal 7. It includes bootstrap's css
No extra configuration is added, only page.tpl & node.tpl
File structure:

        
    ├── README.txt
    ├── css
    │   ├── bootstrap
    │   │   ├── config.json
    │   │   ├── css
    │   │   │   ├── bootstrap-theme.css
    │   │   │   ├── bootstrap-theme.min.css
    │   │   │   ├── bootstrap.css
    │   │   │   └── bootstrap.min.css
    │   │   ├── fonts
    │   │   │   ├── glyphicons-halflings-regular.eot
    │   │   │   ├── glyphicons-halflings-regular.svg
    │   │   │   ├── glyphicons-halflings-regular.ttf
    │   │   │   ├── glyphicons-halflings-regular.woff
    │   │   │   └── glyphicons-halflings-regular.woff2
    │   │   └── js
    │   │       ├── bootstrap.js
    │   │       └── bootstrap.min.js
    │   └── layout.css
    ├── js
    │   └── script.js
    ├── logo.png
    ├── screenshot.png
    ├── simple.info
    └── templates
        ├── node.tpl.php
        └── page.tpl.php

7 directories, 20 files

ABOUT DRUPAL THEMING
--------------------

To learn how to build your own custom theme and override Drupal's default code,
see the Theming Guide: http://drupal.org/theme-guide

See the sites/all/themes/README.txt for more information on where to place your
custom themes to ensure easy maintenance and upgrades.